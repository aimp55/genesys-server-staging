
DROP TABLE IF EXISTS `teammember`;
DROP TABLE IF EXISTS `teaminstitute`;
DROP TABLE IF EXISTS `team`;


CREATE TABLE IF NOT EXISTS `team` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `uuid` varchar(36) NOT NULL,
  `version` bigint(20) not null default 0,
  `createdBy` bigint(20) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `lastModifiedBy` bigint(20) DEFAULT NULL,
  `lastModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `team_UC_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `teammember` (
  `teamId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  PRIMARY KEY (`teamId`, `userId`),
  CONSTRAINT `FK_team_team` FOREIGN KEY (`teamId`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_team_user` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `teaminstitute` (
  `teamId` bigint(20) NOT NULL,
  `instituteId` bigint(20) NOT NULL,
  PRIMARY KEY (`teamId`, `instituteId`),
  CONSTRAINT `FK_teammember_team` FOREIGN KEY (`teamId`) REFERENCES `team` (`id`),
  CONSTRAINT `FK_teammember_institute` FOREIGN KEY (`instituteId`) REFERENCES `faoinstitute` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `user` ADD COLUMN  `uuid` varchar(36) NOT NULL DEFAULT 'changeme' AFTER `id`;
UPDATE `user` set `uuid`=(SELECT UUID()) WHERE `uuid` = 'changeme';

