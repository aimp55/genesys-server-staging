/**
 * Copyright 2014 Global Crop Diversity Trust
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author matijaobreza
 */
public class ReCaptchaUtil {

	private final static Log LOG = LogFactory.getLog(ReCaptchaUtil.class);
	private static final String URL = "https://www.google.com/recaptcha/api/siteverify";

	public static boolean isValid(String reCaptchaResponse, String remoteAddr, String captchaPrivateKey) throws IOException {
		if (reCaptchaResponse == null || "".equals(reCaptchaResponse)) {
			return false;
		}

		boolean isLocalRequest = false;

		URL url = new URL(URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// add reuqest header
		connection.setRequestMethod("POST");

		String postParams = "secret=" + captchaPrivateKey + "&response=" + reCaptchaResponse;

		// Send post request
		connection.setDoOutput(true);
		DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
		dataOutputStream.writeBytes(postParams);
		dataOutputStream.flush();
		dataOutputStream.close();

		try {
			final InetAddress remoteInetAddr = InetAddress.getByName(remoteAddr);
			isLocalRequest = remoteInetAddr.isLinkLocalAddress() || remoteInetAddr.isAnyLocalAddress() || remoteInetAddr.isLoopbackAddress();
			LOG.warn("Remote addr: " + remoteAddr + " " + remoteInetAddr + " isLocal=" + isLocalRequest);
		} catch (final UnknownHostException e1) {
			LOG.warn(e1.getMessage());
		}
		int responseCode = connection.getResponseCode();
		LOG.info("Send recaptcha post request to --> " + url + "\nPost parameters : " + postParams + "\n Response Code : " + responseCode);

		if (isLocalRequest) {
			LOG.info("Ignoring localhost re-captcha.");
			// return true;
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		ObjectMapper objectMapper = new ObjectMapper();
		System.err.println(response.toString());
		JsonNode jsonNode = objectMapper.readTree(response.toString());

		return jsonNode.findValue("success").asBoolean();
	}
}
