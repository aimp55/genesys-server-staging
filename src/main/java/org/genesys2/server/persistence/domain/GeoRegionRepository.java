package org.genesys2.server.persistence.domain;

import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.GeoRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GeoRegionRepository extends JpaRepository<GeoRegion, Long>{

    @Query("select distinct gr from GeoRegion gr where gr.isoCode = ?1")
    GeoRegion findeByIsoCode(String isoCode);

    @Query("select c.region from Country c where c = ?1")
	GeoRegion findByCountry(Country country);
}
