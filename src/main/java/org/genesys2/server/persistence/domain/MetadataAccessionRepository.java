/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;

import org.genesys2.server.model.genesys.Metadata;
import org.genesys2.server.model.genesys.MetadataAccession;
import org.genesys2.server.model.genesys.MetadataMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MetadataAccessionRepository extends JpaRepository<MetadataAccession, Long> {

	List<MetadataMethod> findByMetadata(Metadata metadata);

	@Query("select distinct ma.accessionId from MetadataAccession ma where ma.metadata = ?1")
	List<Long> listMetadataAccessions(Metadata metadata);

	@Query("select distinct ma.metadata from MetadataAccession ma where ma.accessionId = ?1")
	List<Metadata> listMetadataByAccessionId(long accessionId);

	@Modifying
	@Query("delete from MetadataAccession ma where ma.metadata = ?1 and ma.accessionId = ?2")
	void deleteByMetadataAndAccessionId(Metadata metadata, Long id);

}
