/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

public interface FilterConstants {

	public static final String CROPS = "crops";

	public static final String SAMPSTAT = "sampStat";

	public static final String STORAGE = "storage";

	public static final String COLLMISSID = "coll.collMissId";

	public static final String AVAILABLE = "available";

	public static final String ART15 = "art15";

	public static final String MLSSTATUS = "mlsStatus";

	public static final String SGSV = "sgsv";

	public static final String ALIAS = "alias";

	public static final String ACCENUMB = "acceNumb";

	public static final String INSTCODE = "institute.code";

	public static final String GEO_ELEVATION = "geo.elevation";

	public static final String GEO_LONGITUDE = "geo.longitude";

	public static final String GEO_LATITUDE = "geo.latitude";

	public static final String ORGCTY_ISO3 = "orgCty.iso3";

	public static final String TAXONOMY_SCINAME = "taxonomy.sciName";

	public static final String TAXONOMY_SPECIES = "taxonomy.species";

	public static final String TAXONOMY_GENUS = "taxonomy.genus";

	public static final String TAXONOMY_SUBTAXA = "taxonomy.subtaxa";

	public static final String ID = "id";

	public static final String DUPLSITE = "duplSite";

	public static final String DONORCODE = "donorCode";

	public static final String INSTITUTE_COUNTRY_ISO3 = "institute.country.iso3";

	/// Used in mapping library
	public static final String INSTITUTE_COUNTRY_ISO2 = "institute.country.iso2";

	public static final String INSTITUTE_NETWORK = "institute.networks";

	public static final String IN_SGSV = "inSgsv";

	public static final String TAXONOMY_GENUSSPECIES = "taxonomy.genusSpecies";

	public static final String HISTORIC = "historic";

	public static final String SEQUENTIAL_NUMBER = "seqNo";

}
