package org.genesys2.server.service.impl;


public class EasySMTAException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8571665485953509130L;

	public EasySMTAException(String string, Throwable e) {
		super(string, e);
	}

}
