/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.impl.Article;
import org.genesys2.server.model.impl.ClassPK;
import org.genesys2.server.service.ContentService;
import org.genesys2.spring.RequestAttributeLocaleResolver;
import org.genesys2.spring.ResourceNotFoundException;
import org.genesys2.transifex.client.TransifexException;
import org.genesys2.transifex.client.TransifexService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/content")
public class ArticleController extends BaseController {

	@Autowired
	private ContentService contentService;

	@Autowired(required = false)
	private TransifexService transifexService;

	@Autowired
	private RequestAttributeLocaleResolver localeResolver;

	@Resource
	private Set<String> supportedLocales;

	private static final String defaultLanguage = "en";

	@Autowired
	private ObjectMapper mapper;

	@RequestMapping(value = "/transifex", params = { "post" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String postToTransifex(RedirectAttributes redirectAttrs, @RequestParam("slug") String slug,
			@RequestParam(value = "targetId", required = false) Long targetId, @RequestParam("classPkShortName") String classPkShortName) {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		Article article;
		String resourceName;
		if (targetId != null) {
			article = contentService.getArticleBySlugLangTargetIdClassPk(slug, getLocale().getLanguage(), targetId, classPkShortName);
			resourceName = "article-" + slug + "-" + classPkShortName + "-" + targetId;
		} else {
			article = contentService.getGlobalArticle(slug, getLocale());
			resourceName = "article-" + slug;
		}

		String body = String.format("<div class=\"summary\">%s</div><div class=\"body\">%s</div>", article.getSummary(), article.getBody());

		try {
			if (transifexService.resourceExists(resourceName)) {
				transifexService.updateXhtmlResource(resourceName, article.getTitle(), body);
			} else {
				transifexService.createXhtmlResource(resourceName, article.getTitle(), body);
			}
		} catch (IOException e) {
			redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-failed");
			e.printStackTrace();
		}
		redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-resource-updated");

		return "redirect:/content/edit/" + (targetId == null ? slug : slug + "/" + classPkShortName + "/" + targetId) + "/"
				+ LocaleContextHolder.getLocale().getLanguage();
	}

	@RequestMapping(value = "/transifex", params = { "remove" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String deleteFromTransifex(RedirectAttributes redirectAttrs, @RequestParam("slug") String slug,
			@RequestParam(value = "targetId", required = false) Long targetId, @RequestParam("classPkShortName") String classPkShortName) {
		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		String resourceName;
		if (targetId != null) {
			resourceName = "article-" + slug + "-" + classPkShortName + "-" + targetId;
		} else {
			resourceName = "article-" + slug;
		}

		try {
			if (transifexService.deleteResource(resourceName)) {
				redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-resource-removed");
			} else {
				redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-failed");
			}
		} catch (TransifexException e) {
			_logger.error(e.getMessage(), e);
			redirectAttrs.addFlashAttribute("responseFromTransifex", "article.transifex-failed");
		}

		return "redirect:/content/edit/" + (targetId == null ? slug : slug + "/" + classPkShortName + "/" + targetId) + "/"
				+ LocaleContextHolder.getLocale().getLanguage();
	}

	/**
	 * Fetch all from Transifex and store
	 *
	 * @param slug
	 * @param language
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/transifex", params = { "fetch-all" }, method = RequestMethod.POST)
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String fetchAllFromTransifex(RedirectAttributes redirectAttrs, @RequestParam("slug") String slug,
			@RequestParam(value = "targetId", required = false) Long targetId,
			@RequestParam(value = "classPkShortName", required = false) String classPkShortName) throws Exception {

		if (transifexService == null) {
			throw new ResourceNotFoundException("translationService not enabled");
		}

		String resourceName;
		if (targetId != null) {
			resourceName = "article-" + slug + "-" + classPkShortName + "-" + targetId;
		} else {
			resourceName = "article-" + slug;
		}

		List<String> responses = new ArrayList<String>(20);

		for (String lang : supportedLocales) {
			if (defaultLanguage.equalsIgnoreCase(lang)) {
				continue;
			}

			Locale locale = Locale.forLanguageTag(lang);
			_logger.info("Fetching article " + resourceName + " translation for " + locale);

			String translatedResource;
			try {
				translatedResource = transifexService.getTranslatedResource(resourceName, locale);
			} catch (TransifexException e) {
				_logger.warn(e.getMessage(), e);
				if (e.getCause() != null) {
					responses.add(e.getLocalizedMessage() + ": " + e.getCause().getLocalizedMessage());
				} else {
					responses.add(e.getLocalizedMessage());
				}
				// throw new Exception(e.getMessage(), e);
				continue;
			}

			String title;
			String body;
			String summary = null;

			try {
				JsonNode jsonObject = mapper.readTree(translatedResource);
				String content = jsonObject.get("content").asText();

				Document doc = Jsoup.parse(content);
				title = doc.title();
				if (content.contains("class=\"summary")) {
					// 1st <div class="summary">...
					summary = doc.body().child(0).html();
					// 2nd <div class="body">...
					body = doc.body().child(1).html();
				} else {
					// Old fashioned body-only approach
					body = doc.body().html();
				}

				if (targetId != null && StringUtils.isNotBlank(classPkShortName)) {
					ClassPK classPk = contentService.getClassPk(classPkShortName);
					contentService.updateArticle(Class.forName(classPk.getClassName()), targetId, slug, title, body, summary, new Locale(lang));
					responses.add("article.translations-updated");
				} else if (targetId == null && StringUtils.isBlank(classPkShortName)) {
					contentService.updateGlobalArticle(slug, locale, title, body, summary);
					responses.add("article.translations-updated");
				} else {
					responses.add("Error updating local content");
				}
			} catch (IOException e) {
				_logger.warn(e.getMessage(), e);
				responses.add(e.getMessage());
				continue;
			}
		}

		redirectAttrs.addFlashAttribute("responseFromTransifex", responses);

		return "redirect:/content/"
				+ (targetId == null ? slug : slug + "/" + classPkShortName + "/" + targetId + "/" + LocaleContextHolder.getLocale().getLanguage());
	}

	@RequestMapping
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	public String list(ModelMap model, @RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "language", defaultValue = "") String lang) {

		if (!lang.isEmpty()) {
			model.addAttribute("pagedData", contentService.listArticlesByLang(lang, new PageRequest(page - 1, 50, new Sort("slug"))));
		} else {
			model.addAttribute("pagedData", contentService.listArticles(new PageRequest(page - 1, 50, new Sort("slug"))));
		}

		// todo full name of locales
		Map<String, String> locales = new TreeMap<>();

		for (String language : localeResolver.getSupportedLocales()) {
			locales.put(new Locale(language).getDisplayName(), language);
		}

		model.addAttribute("languages", locales);
		model.addAttribute("language", lang);

		return "/content/index";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("/{slug:.+}/{classPkShortName}/{targetId:\\d+}/{language}")
	public String viewScoped(ModelMap model, @PathVariable(value = "slug") String slug, @PathVariable("classPkShortName") String classPkShortName,
			@PathVariable("targetId") Long targetId, @PathVariable("language") String language) {
		_logger.debug("Viewing article " + slug);

		final Article article = contentService.getArticleBySlugLangTargetIdClassPk(slug, language, targetId, classPkShortName);

		if (article == null) {
			if (hasRole("ADMINISTRATOR")) {
				return "redirect:/content/edit/" + slug + "/" + classPkShortName + "/" + targetId + "/" + language;
			}
			throw new ResourceNotFoundException();
		}
		model.addAttribute("title", article.getTitle());
		model.addAttribute("article", article);

		return "/content/article";
	}

	@RequestMapping("{slug:.+}")
	public String view(ModelMap model, @PathVariable(value = "slug") String slug) {
		_logger.debug("Viewing article " + slug);

		final Article article = contentService.getGlobalArticle(slug, getLocale());
		if (article == null) {
			if (hasRole("ADMINISTRATOR")) {
				return "redirect:/content/edit/" + slug + "/" + LocaleContextHolder.getLocale().getLanguage();
			}
			throw new ResourceNotFoundException();
		}
		model.addAttribute("title", article.getTitle());
		model.addAttribute("article", article);

		return "/content/article";
	}

	@RequestMapping("{menu}/{url:.+}")
	public String viewWithMenu(ModelMap model, @PathVariable(value = "menu") String menuKey, @PathVariable(value = "url") String slug) {
		String result = view(model, slug);
		if (StringUtils.isNotBlank(menuKey)) {
			_logger.debug("Loading menu " + menuKey);
			model.addAttribute("menu", contentService.getMenu(menuKey));
		}
		return result;
	}

	/**
	 * Edit article in current language
	 * 
	 * @param model
	 * @param slug
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("/edit/{url:.+}")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug) {
		_logger.debug("Editing article " + slug);
		return "redirect:/content/" + slug + "/edit/" + LocaleContextHolder.getLocale().getLanguage();
	}

	/**
	 * Edit article in another language
	 * 
	 * @param model
	 * @param slug
	 * @param language
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("/edit/{url}/{language}")
	public String edit(ModelMap model, @PathVariable(value = "url") String slug, @PathVariable("language") String language) {
		_logger.debug("Editing article " + slug);

		Article article = contentService.getArticleBySlugAndLang(slug, language);
		if (article == null) {
			article = new Article();
			article.setSlug(slug);
			article.setLang(language);
		}
		model.addAttribute("article", article);

		return "/content/article-edit";
	}

	/**
	 * Edit article in another language
	 *
	 * @param model
	 * @param slug
	 * @param shortName
	 * @param targetId
	 * @param language
	 * @return
	 */
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping("/edit/{slug}/{classPkShortName}/{targetId}/{language}")
	public String edit(ModelMap model, @PathVariable(value = "slug") String slug, @PathVariable("classPkShortName") String classPkShortName,
			@PathVariable("targetId") Long targetId, @PathVariable("language") String language) {
		_logger.debug("Editing article " + slug);

		Article article = contentService.getArticleBySlugLangTargetIdClassPk(slug, language, targetId, classPkShortName);

		if (article == null) {
			article = new Article();
			article.setSlug(slug);
			article.setLang(language);
			article.setTargetId(targetId);
			article.setClassPk(contentService.getClassPk(classPkShortName));
		}
		model.addAttribute("article", article);

		return "/content/article-edit";
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", method = { RequestMethod.POST })
	public String createNewGlobalArticle(ModelMap model, @RequestParam("slug") String slug, @PathVariable("language") String language,
			@RequestParam("title") String title, @RequestParam("body") String body, @RequestParam(value = "summary", required = false) String summary) {

		Article article = contentService.updateGlobalArticle(slug, new Locale(language), title, body, summary);
		return redirectAfterSave(article, language);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", params = { "id" }, method = { RequestMethod.POST })
	public String saveExistingGlobalArticle(ModelMap model, @PathVariable("language") String language, @RequestParam("id") long id,
			@RequestParam("slug") String slug, @RequestParam("title") String title, @RequestParam("body") String body,
			@RequestParam(value = "summary", required = false) String summary) {

		Article article = contentService.updateArticle(id, slug, title, body, summary);
		return redirectAfterSave(article, language);
	}

	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER')")
	@RequestMapping(value = "/save-article/{language}", params = { "classPkShortName", "targetId" }, method = { RequestMethod.POST })
	public String saveNewClassPkArticle(ModelMap model, @PathVariable("language") String language, @RequestParam("classPkShortName") String classPkShortName,
			@RequestParam("targetId") long entityId, @RequestParam("slug") String slug, @RequestParam("title") String title, @RequestParam("body") String body,
			@RequestParam(value = "summary", required = false) String summary) throws ClassNotFoundException {

		ClassPK classPk = contentService.getClassPk(classPkShortName);
		Article article = contentService.updateArticle(Class.forName(classPk.getClassName()), entityId, slug, title, body, summary, new Locale(language));
		return redirectAfterSave(article, language);
	}

	private String redirectAfterSave(Article article, String language) {
		if (LocaleContextHolder.getLocale().getLanguage().equals(language)) {
			return "redirect:/content";
		} else {
			return "redirect:/content?language=" + language;
		}
	}
}
