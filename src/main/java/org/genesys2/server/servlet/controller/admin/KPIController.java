package org.genesys2.server.servlet.controller.admin;

import java.util.Map;

import org.genesys2.server.model.kpi.Execution;
import org.genesys2.server.model.kpi.ExecutionRun;
import org.genesys2.server.service.KPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("kpiAdminController")
@RequestMapping(value = "/admin/kpi")
public class KPIController {

	@Autowired
	private KPIService kpiService;

	@Value("${paginator.default.pageSize}")
	private int pageSize;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model) {
		model.addAttribute("executions", kpiService.listExecutions());
		return "/admin/kpi/index";
	}

	@RequestMapping(value = "/exec/{executionName:.+}", method = RequestMethod.GET)
	public String execution(ModelMap model, @PathVariable("executionName") String executionName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		Execution execution = kpiService.getExecution(executionName);
		model.addAttribute("execution", execution);
		model.addAttribute("executionRuns", kpiService.listExecutionRuns(execution, new PageRequest(page - 1, pageSize)));
		return "/admin/kpi/execution";
	}
	

	@RequestMapping(value = "/exec/{executionName:.+}", method = RequestMethod.GET, params={"dk"})
	public String execution(ModelMap model, @PathVariable("executionName") String executionName,
			@RequestParam(value = "dk", required = true) long dimensionKeyId) {
		Execution execution = kpiService.getExecution(executionName);
		model.addAttribute("execution", execution);
		model.addAttribute("observations", kpiService.listObservations(execution, dimensionKeyId, new PageRequest(0, pageSize)));
		return "/admin/kpi/execution";
	}

	@RequestMapping(value = "/exec/{executionName}/run/{runId}", method = RequestMethod.GET)
	public String executionRun(ModelMap model, @PathVariable("executionName") String executionName, @PathVariable("runId") long runId,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		Execution execution = kpiService.getExecution(executionName);
		ExecutionRun executionRun = kpiService.getExecutionRun(runId);
		model.addAttribute("execution", execution);
		model.addAttribute("executionRun", executionRun);
		Map<String, String> dimensionFilters = null;
		model.addAttribute("observations", kpiService.listObservations(executionRun, dimensionFilters, new PageRequest(page - 1, 500)));

		return "/admin/kpi/run";
	}
}
