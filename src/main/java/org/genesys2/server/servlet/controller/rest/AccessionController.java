/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.json.AccessionJson;
import org.genesys2.server.model.json.Api1Constants;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysRESTService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.server.service.impl.RESTApiException;
import org.genesys2.server.service.impl.RESTApiValueException;
import org.genesys2.server.service.impl.SearchException;
import org.genesys2.server.servlet.controller.rest.model.AccessionHeaderJson;
import org.genesys2.server.servlet.controller.rest.model.AccessionNamesJson;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.orm.hibernate4.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Controller("restAccessionController")
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0/acn", "/json/v0/acn" })
public class AccessionController extends RestController {

	private final ObjectMapper mapper = new ObjectMapper();

	@Autowired
	GenesysService genesysService;

	@Autowired
	private GenesysFilterService filterService;

	@Autowired
	private CropService cropService;

	@Autowired
	BatchRESTService batchRESTService;

	@Autowired
	InstituteService instituteService;

	@Autowired
	GeoService geoService;

	@Autowired
	TaxonomyService taxonomyService;

	@Autowired
	GenesysRESTService restService;

	@Autowired
	ElasticService elasticService;

	/**
	 * Check if accessions exists in the system
	 * 
	 * @return
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/exists/{instCode}/{genus:.+}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody boolean exists(@PathVariable("instCode") String instCode, @PathVariable("genus") String genus,
			@RequestParam("acceNumb") String acceNumb) throws NonUniqueAccessionException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Checking if accn exists " + instCode + "." + acceNumb + " genus=" + genus);
		}

		final Accession accession = genesysService.getAccession(instCode, acceNumb, genus);

		if (accession == null) {
			LOG.warn("No accession " + instCode + "." + acceNumb + " genus=" + genus);
		}

		return accession != null;
	}

	/**
	 * Check if accessions exists in the system
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws RESTApiValueException
	 * @throws NonUniqueAccessionException
	 */
	@RequestMapping(value = "/{instCode}/check", method = { RequestMethod.POST, RequestMethod.PUT }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody JsonNode check(@PathVariable("instCode") String instCode, @RequestBody String content)
			throws JsonProcessingException, IOException, RESTApiValueException, NonUniqueAccessionException {
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException("No institute " + instCode);
		}

		final JsonNode json = mapper.readTree(content);
		final List<String> batch = new ArrayList<String>();

		if (json.isArray()) {
			for (final JsonNode j : json) {
				if (j.isNull() || !j.isTextual()) {
					throw new RESTApiValueException("acceNumb must be a non-null String");
				}
				batch.add(j.textValue());
			}
		} else {
			if (json.isNull() || !json.isTextual()) {
				throw new RESTApiValueException("acceNumb must be a non-null String");
			}
			batch.add(json.textValue());
		}

		LOG.info("Batch processing " + batch.size() + " entries");
		final ArrayNode rets = mapper.createArrayNode();

		for (final String acceNumb : batch) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Loading accession " + acceNumb + " from " + instCode);
			}

			if (institute.hasUniqueAcceNumbs()) {
				final Accession accession = genesysService.getAccession(instCode, acceNumb);
				rets.add(accession == null ? null : accession.getAccessionId().getId());
			} else {
				final List<Accession> accessions = genesysService.listAccessions(institute, acceNumb);
				if (accessions.size() == 1) {
					rets.add(accessions.get(0).getAccessionId().getId());
				} else {
					final ArrayNode ret = rets.arrayNode();
					for (final Accession accession : accessions) {
						ret.add(accession.getAccessionId().getId());
					}
					rets.add(ret);
				}
			}
		}

		return rets;
	}

	/**
	 * Update accessions in the system
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws RESTApiException
	 */
	@RequestMapping(value = "/{instCode}/upsert", method = { RequestMethod.POST, RequestMethod.PUT }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String upsertInstituteAccession(@PathVariable("instCode") String instCode, @RequestBody String content)
			throws JsonProcessingException, IOException, RESTApiException {
		// User's permission to WRITE to this WIEWS institute are checked in
		// BatchRESTService.
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException();
		}

		final JsonNode json = mapper.readTree(content);
		final Map<AccessionHeaderJson, ObjectNode> batch = new HashMap<AccessionHeaderJson, ObjectNode>();

		if (json.isArray()) {
			for (final JsonNode j : json) {
				final AccessionHeaderJson dataJson = readAid3(j);
				if (!instCode.equals(dataJson.instCode)) {
					throw new RuntimeException("Accession does not belong to instCode=" + instCode + " acn=" + dataJson);
				}
				batch.put(dataJson, (ObjectNode) j);
			}
		} else {
			final AccessionHeaderJson dataJson = readAid3(json);
			if (!instCode.equals(dataJson.instCode)) {
				throw new RuntimeException("Accession does not belong to instCode=" + instCode + " acn=" + dataJson);
			}
			batch.put(dataJson, (ObjectNode) json);
		}

		Throwable cause = null;

		for (int i = 0; i < 10; i++) {
			try {
				// Step 1: Ensure all taxonomic data provided by client is
				// persisted
				batchRESTService.ensureTaxonomies(institute, batch);
				// Step 2: Upsert data
				batchRESTService.upsertAccessionData(institute, batch);
				// Force update institute#accessionCount (outside previous
				// transaction)
				genesysService.updateAccessionCount(institute);
				return JSON_OK;
			} catch (PleaseRetryException | HibernateOptimisticLockingFailureException
					| org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException e) {
				LOG.info("Will retry upsert. Error: " + e.getMessage());
				cause = e;
			}
		}

		throw new RESTApiException("Could not upsert accession data.", cause);
	}

	/**
	 * Update accessions in the system
	 * 
	 * @return
	 * @throws RESTApiException
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/{instCode}/names", method = { RequestMethod.POST, RequestMethod.PUT }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String upsertAccessionNames(@PathVariable("instCode") String instCode, @RequestBody List<AccessionNamesJson> batch)
			throws RESTApiException {
		// User's permission to WRITE to this WIEWS institute are checked in
		// BatchRESTService.
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException();
		}

		for (final AccessionNamesJson aid3 : batch) {
			if (!instCode.equals(aid3.instCode)) {
				throw new RuntimeException("Accession does not belong to instCode=" + instCode + " acn=" + aid3);
			}
		}

		batchRESTService.upsertAccessionNames(institute, batch);
		return JSON_OK;
	}

	/**
	 * Delete accessions by acceNumb (and genus)?
	 * 
	 * @return
	 * @throws RESTApiException
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/{instCode}/delete-named", method = { RequestMethod.POST, RequestMethod.PUT }, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody JsonDeleteResult deleteAccessions(@PathVariable("instCode") String instCode, @RequestBody List<AccessionHeaderJson> batch)
			throws RESTApiException {
		// User's permission to WRITE to this WIEWS institute are checked in
		// BatchRESTService.
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException();
		}

		try {
			final int deleted = batchRESTService.deleteAccessions(institute, batch);
			LOG.info("Deleted " + deleted + " accessions from " + instCode);
			genesysService.updateAccessionCount(institute);
			return new JsonDeleteResult(deleted);
		} catch (CannotAcquireLockException e) {
			throw new PleaseRetryException("Operation failed, please retry.", e);
		}
	}

	/**
	 * Delete accessions by acceNumb (and genus)?
	 * 
	 * @return
	 * @throws RESTApiException
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = "/{instCode}/delete", method = { RequestMethod.POST }, consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody JsonDeleteResult deleteAccessionsById(@PathVariable("instCode") String instCode, @RequestBody List<Long> batch)
			throws RESTApiException {
		// User's permission to WRITE to this WIEWS institute are checked in
		// BatchRESTService.
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException();
		}

		final int deleted = batchRESTService.deleteAccessionsById(institute, batch);
		LOG.info("Deleted " + deleted + " accessions from " + instCode);
		genesysService.updateAccessionCount(institute);
		return new JsonDeleteResult(deleted);
	}

	@RequestMapping(value = "/search", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Page<AccessionDetails> search(@RequestParam("page") final int page, @RequestParam("query") final String query) throws SearchException {
		return elasticService.search(StringUtils.defaultIfBlank(query, "*"), new PageRequest(page - 1, 20));
	}

	// FIXME Not using institute...
	@RequestMapping(value = "/{instCode}/list", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody Page<AccessionDetails> list(@PathVariable("instCode") String instCode, @RequestParam("page") int page,
			@RequestParam("query") String query) throws SearchException {
		FaoInstitute institute = instituteService.getInstitute(instCode);
		if (institute == null) {
			throw new ResourceNotFoundException();
		}
		query = StringUtils.defaultIfBlank(query, "*");
		return elasticService.search(query, new PageRequest(page - 1, 20));
	}

	@RequestMapping(value = "/{id}", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody AccessionDetails getDetails(@PathVariable("id") long accessionId) {

		final AccessionDetails ad = genesysService.getAccessionDetails(accessionId);
		if (ad == null) {
			throw new ResourceNotFoundException();
		}

		return ad;
	}

	@RequestMapping(value = "/list", method = { RequestMethod.POST }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody List<AccessionJson> get(@RequestBody Set<Long> accessionIds) {
		return restService.getAccessionJSON(accessionIds);
	}

	@RequestMapping(value = "/filter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getAcc(@RequestBody JsonData jsonData) throws IOException {

		AppliedFilters appliedFilters = new AppliedFilters();
		AppliedFilter cropFilter = new AppliedFilter().setFilterName(FilterConstants.CROPS);
		cropFilter.addFilterValue(new FilterHandler.LiteralValueFilter(jsonData.crop));
		appliedFilters.add(cropFilter);

		if (LOG.isDebugEnabled()) {
			LOG.debug(appliedFilters.toString());
		}

		Page<Accession> accessions = filterService.listAccessions(appliedFilters,
				new PageRequest(jsonData.startAt - 1, jsonData.maxRecords, new Sort("acceNumb")));
		return accessions;
	}

	public static class JsonData {
		public String crop;
		public Integer startAt;
		public Integer maxRecords;
		public String otherOptions;
	}

	private AccessionHeaderJson readAid3(JsonNode json) {
		final AccessionHeaderJson dataJson = new AccessionHeaderJson();

		dataJson.instCode = json.has(Api1Constants.Accession.INSTCODE) ? json.get(Api1Constants.Accession.INSTCODE).textValue() : null;
		dataJson.acceNumb = json.has(Api1Constants.Accession.ACCENUMB) ? json.get(Api1Constants.Accession.ACCENUMB).textValue() : null;
		dataJson.genus = json.has(Api1Constants.Accession.GENUS) ? json.get(Api1Constants.Accession.GENUS).textValue() : null;

		return dataJson;
	}

	public static final class JsonDeleteResult {
		private final int deleted;

		public JsonDeleteResult(int deleted) {
			this.deleted = deleted;
		}

		public int getDeleted() {
			return deleted;
		}

		public boolean getResult() {
			return true;
		}
	}
}
