package org.genesys2.server.model.dataset;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ds2value", indexes = { @Index(columnList = "r,dsd"), @Index(columnList = "dsd,vall"), @Index(columnList = "dsd,vald") })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.INTEGER, name = "typ")
public abstract class DSValue<T> {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@ManyToOne(fetch=FetchType.LAZY, optional = false)
	@JoinColumn(name = "r")
	private DSRow row;

	@ManyToOne(fetch=FetchType.LAZY, optional = false)
	@JoinColumn(name = "dsd")
	private DSDescriptor datasetDescriptor;

	public abstract T getValue();

	public abstract void setValue(T value);

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DSRow getRow() {
		return row;
	}

	public void setRow(DSRow row) {
		this.row = row;
	}

	public DSDescriptor getDatasetDescriptor() {
		return datasetDescriptor;
	}

	public void setDatasetDescriptor(DSDescriptor datasetDescriptor) {
		this.datasetDescriptor = datasetDescriptor;
	}

	public static DSValue<?> make(Object v) {
		if (v instanceof Long) {
			return new DSValueLong();
		} else if (v instanceof Double) {
			return new DSValueDouble();
		} else if (v instanceof String) {
			return new DSValueString();
		}
		return null;
	}

	public void setValue2(Object v) {
		setValue((T) v);
	}
}
