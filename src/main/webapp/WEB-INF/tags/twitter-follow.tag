<%@ tag description="Follow somebody on Twitter" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ attribute name="screenName" required="true" type="java.lang.String" description="Follow this Twitter name" %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="regular or large" %>

<c:url var="twatUrl" value="https://twitter.com/intent/follow">
  <c:param name="screen_name" value="${screenName}" />
</c:url>
<a class="twitter-follow-button" target="_blank" href="${twatUrl}" data-size="${dataSize}" dir="ltr">
<i class="fa fa-twitter"></i> @<c:out value="${screenName}" />
<%-- <spring:message code="twitter.follow-X" arguments="${screenName}" /> --%></a>
