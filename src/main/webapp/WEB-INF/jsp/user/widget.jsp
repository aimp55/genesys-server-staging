<%@ page contentType=" ISO-8859-1ISO-8859-1;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Your Plugin Code</h4>
</div>
    <div class="modal-body" style="overflow: auto;height: 3--px">
    <c:if test="${script ne null}">
        <textarea  id="area" style="min-width: 535px;min-height: 200px">${script}</textarea>
    </c:if>
        <label for="clients">Select client details</label>
        <select  name="clients" id="clientselector" class="get_widget form-control" >
            <c:forEach items="${clientDetails}" var="detail">
                <option class="get_widget"  ${detail.clientId== client.clientId?'selected':''}>${detail.clientId}</option>
            </c:forEach>
        </select>
</div>
    <div class="modal-footer">
    <button type="button" class="btn btn-primary" data-dismiss="modal"><spring:message code="filter.apply"/></button>
</div>




