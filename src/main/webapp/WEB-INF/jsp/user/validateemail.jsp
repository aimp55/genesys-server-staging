<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="validate.email"/></title>
</head>
<body>
<h1>
    <spring:message code="validate.email.key"/>
</h1>

<form role="form" class="form-vertical validate" action="<c:url value="/profile/${tokenUuid}/validate" />" method="post">

    <div class="col-lg-3">
        <input type="text" id="key" name="key" class="span1 form-control" maxlength="4"/>
    </div>
    <div class="col-lg-3">
        <input type="submit" value="OK" class="btn btn-primary"/>
    </div>

    <c:if test="${error ne null }">
        <div class="alert alert-error">
            <spring:message code="validate.email.invalid.key"/>
        </div>
    </c:if>
    <!-- CSRF protection -->
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>


</body>
</html>