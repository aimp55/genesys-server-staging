/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.wrapper.UserWrapper;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.spring.SecurityContextUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserServiceTest extends AbstractServicesTest {

	private static final Log LOG = LogFactory.getLog(UserServiceTest.class);

	private String email;

	private String initialPassword;

	private String fullName;

	private User user;

	@Before
	public void setUp() {
		email = "test@example.com";
		initialPassword = "Alexandr19011990";
		fullName = "Alechandro";

		user = new User();

		user.setEmail(email);
		user.setPassword(initialPassword);
		user.setName(fullName);
	}

	@After
	public void teardown() {

		try {
			if (userService.exists(email)) {
				User forRemoveUser = userService.getUserByEmail(email);
				userService.removeUser(forRemoveUser);
			}
		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void listAvailableRolesTest() {
		LOG.info("Start test-method listAvailableRolesTest");

		assertTrue(userService.listAvailableRoles().size() == UserRole.values().length);

		LOG.info("Test listAvailableRolesTest passed!");
	}

	@Test
	public void getUserdetailsTest() {
		LOG.info("Start test-method getUserdetailsTest");

		try {
			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			AuthUserDetails userDetails = (AuthUserDetails) userService.getUserDetails(user);
			assertTrue(userDetails != null);
			assertTrue(userDetails.getUser() == user);

		} catch (UserException e) {
			e.printStackTrace();
		}
		LOG.info("Test getUserdetailsTest passed!");
	}

	// @Ignore
	@Test
	public void listUsersTest() {
		LOG.info("Start test-method listUsersTest");

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			Pageable pageable = (Pageable) new PageRequest(0, 10, new Sort("email"));
			List<User> users = userService.listUsers(pageable).getContent();
			assertTrue(users.size() == 1);

		} catch (UserException e) {
			e.printStackTrace();
		}
		LOG.info("Test listUsersTest passed!");
	}

	@Test
	public void getWrappedByIdTest() {
		LOG.info("Start test-method getWrappedByIdTest");

		try {
			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			UserWrapper userWrapper = userService.getWrappedById(user.getId());
			assertTrue(userWrapper != null);

		} catch (UserException e) {
			e.printStackTrace();
		}

		LOG.info("Test getWrappedByIdTest passed!");
	}

	@Test
	public void listWrappedTest() {
		LOG.info("Start test-method listWrappedTest");

		int startRow = 0;
		int pageSize = 10;

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			Page<UserWrapper> userWrappers = userService.listWrapped(startRow, pageSize);
			assertTrue(userWrappers != null);
			assertTrue(userWrappers.getContent().get(0).getEmail().equals(email));

		} catch (UserException e) {
			e.printStackTrace();
		}

		LOG.info("Test listWrappedTest passed!");
	}

	@Test
	public void createAccountTest() {

		LOG.info("Start test-method createAccountTest");

		User newUser = userService.createAccount(email, initialPassword, fullName);

		assertNotNull("User is ull", newUser);

		assertTrue(newUser.getId() != null);

		assertTrue(newUser.getEmail().equals(email));

		assertTrue(newUser.getName().equals(fullName));

		assertTrue(newUser.getPassword().equals(initialPassword));

		LOG.info("Test createAccountTest Passed!");

	}

	@Test
	public void addUserTest() {
		LOG.info("Start test-method addUserTest");

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test addUserTest passed!");
	}

	@Test
	public void updateUserTest() {
		LOG.info("Start test-method updateUserTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			User userFromDB = userService.getUserByEmail(email);
			userFromDB.setPassword("NewPassword");
			userFromDB.setName("New Name");
			userService.updateUser(userFromDB);

			assertTrue(userService.getUserByEmail(email).getPassword().equals("NewPassword"));
			assertTrue(userService.getUserByEmail(email).getName().equals("New Name"));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updateUserTest passed!");
	}

	@Test
	public void updateDataTest() {
		LOG.info("Start test-method updateDataTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			User userFromDB = userService.getUserByEmail(email);

			fullName = "This is new name";
			email = "newMail@new.com";

			userService.updateData(userFromDB.getId(), fullName, email);
			System.out.println(userService.getUserByEmail(email).getEmail());

			userFromDB = userService.getUserByEmail(email);
			assertTrue(userFromDB.getName().equals(fullName));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updateDataTest passed!");
	}

	@Test
	public void updatePassword() {
		LOG.info("Start test-method updatePassword");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			User userFromDB = userService.getUserByEmail(email);

			assertTrue(userService.getUserByEmail(email).getPassword().equals(initialPassword));
			initialPassword = "This is new password";
			userService.updatePassword(userFromDB.getId(), initialPassword);

			userFromDB = userService.getUserByEmail(email);
			assertTrue(userFromDB.getPassword().equals(initialPassword));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updatePassword passed!");
	}

	@Test
	public void setAccountEnabledTest() {
		LOG.info("Start test-method setAccountEnabledTest");
		boolean enabled = true;
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			assertTrue(userService.getUserByUuid(user.getUuid()) != null);
			userService.setAccountEnabled(user.getUuid(), enabled);
			assertTrue((userService.getUserByUuid(user.getUuid())).isEnabled());

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test setAccountEnabledTest passed!");
	}

	@Test(expected = SecurityException.class)
	public void setDesabledAdminTest() {
		LOG.info("Start test-method setDesabledAdminTest");
		boolean enabled = false;
		user.getRoles().add(UserRole.ADMINISTRATOR);

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			userService.setAccountEnabled(user.getUuid(), enabled);

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test setDesabledAdminTest passed!");
	}

	@Test
	public void setAccountLockLocalTest() {
		LOG.info("Start test-method setAccountLockLocalTest");
		boolean locked = true;

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			userService.setAccountLockLocal(user.getUuid(), locked);
			assertTrue(userService.getUserByEmail(email).isAccountLocked());

			locked = false;
			userService.setAccountLockLocal(user.getUuid(), locked);
			assertFalse(userService.getUserByEmail(email).isAccountLocked());

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test setAccountLockLocalTest passed!");
	}

	@Test
	public void removeUserTest() {
		LOG.info("Start test-method removeUserTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			userService.removeUser(user);
			assertFalse(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test removeUserTest passed!");
	}

	@Test
	public void removeUserByIdTest() {
		LOG.info("Start test-method removeUserByIdTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			userService.removeUserById(user.getId());
			assertFalse(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test removeUserByIdTest passed!");
	}

	@Test(expected = NullPointerException.class)
	public void getMeWithoutAuthTest() {
		LOG.info("Start test-method getMeWithoutAuthTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			AuthUserDetails authUserDetails = SecurityContextUtil.getAuthUser();
			authUserDetails.setUser(user);
			assertTrue(userService.getMe().getUuid().equals(user.getUuid()));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getMeWithoutAuthTest passed!");
	}

	@Test
	public void getUserByEmailTest() {
		LOG.info("Start test-method getUserByEmailTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getUserByEmailTest passed!");
	}

	@Test
	public void getUserByUuidTest() {
		LOG.info("Start test-method getUserByUuidTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			assertTrue(userService.getUserByUuid(user.getUuid()) != null);
			assertTrue(userService.getUserByUuid(user.getUuid()).getUuid().equals(user.getUuid()));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getUserByUuidTest passed!");
	}

	@Test(expected = UsernameNotFoundException.class)
	public void getUserByUuidExceptionTest() {
		LOG.info("Start test-method getUserByUuidExceptionTest");

		assertFalse(userService.exists(fullName));
		userService.getUserByUuid(user.getUuid());

		LOG.info("Test getUserByUuidExceptionTest passed!");
	}

	@Test
	public void getUserByIdTest() {
		LOG.info("Start test-method getUserByIdTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			assertTrue(userService.getUserById(user.getId()) != null);

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getUserByIdTest passed!");
	}

	@Test
	public void existsTest() {
		LOG.info("Start test-method existsTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test existsTest passed!");
	}

	@Test
	public void userEmailValidatedTest() {

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			List<GrantedAuthority> authorities = new ArrayList<>();
			GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
			authorities.add(simpleGrantedAuthority);

			AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

			// set actual DB user
			authUserDetails.setUser(user);

			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

			SecurityContextHolder.getContext().setAuthentication(authToken);

			assertTrue(authUserDetails.getUser() == user);

			userService.updateUser(user);

			assertFalse(userService.getUserByEmail(email).getRoles().contains(UserRole.VALIDATEDUSER));
			userService.userEmailValidated(user.getUuid());
			assertTrue(userService.getUserByEmail(email).getRoles().contains(UserRole.VALIDATEDUSER));

		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void updateRolesTest() {
		LOG.info("Start test-method updateRolesTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			assertTrue(userService.getUserByEmail(email).getRoles().isEmpty());
			List<String> selectedRoles = new ArrayList<>();
			selectedRoles.add(UserRole.ADMINISTRATOR.getName());
			userService.updateRoles(user, selectedRoles);
			assertTrue(userService.getUserByEmail(email).getRoles().contains(UserRole.ADMINISTRATOR));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updateRolesTest passed!");
	}

	@Test
	public void autocompleteUserTest() {
		LOG.info("Start test-method autocompleteUserTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			assertTrue(userService.autocompleteUser("").isEmpty());
			assertTrue(userService.autocompleteUser("m").isEmpty());
			assertTrue(userService.autocompleteUser("ma").isEmpty());
			assertTrue(userService.autocompleteUser("mai").isEmpty());

			assertTrue(userService.autocompleteUser(email).size() == 1);

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test autocompleteUserTest passed!");
	}
}
