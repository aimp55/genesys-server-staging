/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.listener.sample.CreateContentListener;
import org.genesys2.server.model.genesys.MaterialRequest;
import org.genesys2.server.persistence.domain.MaterialRequestRepository;
import org.genesys2.server.security.AsAdminInvoker;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.EMailService;
import org.genesys2.server.service.EasySMTA;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.RequestService;
import org.genesys2.server.service.RequestService.RequestException;
import org.genesys2.server.service.RequestService.RequestInfo;
import org.genesys2.server.service.TokenVerificationService;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.EMailServiceImpl;
import org.genesys2.server.service.impl.EasySMTAMockConnector;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.RequestServiceImpl;
import org.genesys2.server.service.impl.TokenVerificationServiceImpl;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MaterialRequestTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class MaterialRequestTest {

	@Import(GenesysBeansConfig.class)
	public static class Config {

		@Bean
		public TokenVerificationService tokenVerificationService() {
			return new TokenVerificationServiceImpl();
		}

		@Bean
		public EasySMTA easySmta() {
			return new EasySMTAMockConnector();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public RequestService requestService() {
			return new RequestServiceImpl();
		}

		@Bean
		public EMailService emailService() {
			return new EMailServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public ThreadPoolTaskExecutor taskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

		@Bean
		public JavaMailSender javaMailSender() {
			final JavaMailSender jms = new JavaMailSenderImpl() {
				@Override
				protected void doSend(MimeMessage[] mimeMessages, Object[] originalMessages) throws MailException {
					System.err.println("Faking sending email...");
				}
			};
			return jms;
		}

		@Bean
		public AsAdminInvoker asAdminInvoker() {
			return new AsAdminInvoker();
		}

		@Bean
		public CreateContentListener createContentListener() {
			return new CreateContentListener();
		}
	}

	@Autowired
	private RequestService requestService;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private MaterialRequestRepository requestRepository;

	// Initialize bean to create default contents
	@Autowired
	private CreateContentListener createContentListener;

	@Ignore("We don't have accession data")
	@Test
	public void testCreateRequest() {
		final RequestInfo requestInfo = new RequestInfo();
		final Set<Long> accessionIds = generateAccessionIds();

		MaterialRequest request = null;

		try {
			request = requestService.initiateRequest(requestInfo, accessionIds);
		} catch (final RequestException e) {
			fail(e.getMessage());
		}

		System.err.println(request);
		assertTrue(request.getId() != null);
		assertTrue(request.getUuid() != null);
		assertTrue(request.getBody() != null);
		assertTrue(request.getPid().equals("pid1"));
		assertTrue(request.getEmail().equals("email@localhost"));

		final MaterialRequest loaded = requestService.get(request.getUuid());
		assertTrue("Could not load request", loaded != null);
		assertTrue("Got something else back", loaded.getId().equals(request.getId()));
	}

	Set<Long> generateAccessionIds() {
		final Set<Long> accessionIds = new HashSet<Long>();
		for (long i = 2 + RandomUtils.nextInt(20); i >= 0; i--) {
			accessionIds.add(i);
		}
		return accessionIds;
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void expectDataIntegrityViolationOnDoubleUuid() {
		final MaterialRequest mr = new MaterialRequest();
		mr.setPid("pid2");
		mr.setEmail("email@localhost");

		requestRepository.save(mr);
		assertTrue(mr.getId() != null);
		assertTrue(mr.getUuid() != null);

		final MaterialRequest mr2 = new MaterialRequest();
		mr2.setUuid(mr.getUuid());
		requestRepository.save(mr2);
	}

	@Test(expected = RequestException.class)
	public void testEmail() throws RequestException {
		final RequestInfo requestInfo = new RequestInfo();
		final Set<Long> accessionIds = generateAccessionIds();

		requestService.initiateRequest(requestInfo, accessionIds);
	}

	@Test
	public void loadNonExistent() {
		final MaterialRequest loaded = requestService.get("non-existent-uuid");
		assertTrue("Should be null", loaded == null);
	}
}
