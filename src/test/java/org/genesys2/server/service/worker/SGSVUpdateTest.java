/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.worker;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.persistence.domain.SvalbardRepository;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.BatchRESTService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.BatchRESTServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.NonUniqueAccessionException;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.TaxonomyManager;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.hsqldb.lib.StringInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SGSVUpdateTest.Config.class, initializers = PropertyPlacholderInitializer.class)
public class SGSVUpdateTest {

	private static final Log LOG = LogFactory.getLog(SGSVUpdateTest.class);

	@Import(JpaDataConfig.class)
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {
		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public BatchRESTService batchRESTService() {
			return new BatchRESTServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public SGSVUpdate sgsvUpdate() {
			return new SGSVUpdate();
		}

		@Bean
		public TaskExecutor taskExecutor() {
			return new ThreadPoolTaskExecutor();
		}

	}

	@Autowired
	private SGSVUpdate sgsvUpdate;

	@Autowired
	private InstituteService instituteService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private SvalbardRepository svalbardRepository;

	@Autowired
	private TaxonomyService taxonomyService;

	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private TaxonomyManager taxonomyManager;

	@Before
	public void setup() {
		LOG.info("Setting up");
		final Collection<FaoInstitute> institutes = new ArrayList<FaoInstitute>();
		for (final String instCode : new String[] { "INS001", "INS002" }) {
			final FaoInstitute institute = new FaoInstitute();
			institute.setFullName(instCode + " institute");
			institute.setCode(instCode);
			institute.setUniqueAcceNumbs(true);
			institutes.add(institute);
		}
		instituteService.update(institutes);
	}

	@After
	public void afterEachTest() {
		LOG.info("Deleting institutes");
		instituteService.delete("INS001");
		instituteService.delete("INS002");
		assertTrue(instituteService.list(new PageRequest(0, 1)).getNumber() == 0);
	}

	private void makeAccession(String instCode, String acceNumb, String genus, String species) {
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		final List<Accession> accessions = new ArrayList<Accession>();

		final Accession a = new Accession();
		a.setInstitute(institute);
		a.setAccessionName(acceNumb);
		a.setTaxonomy(taxonomyManager.ensureTaxonomy2(genus, species, null, null, null));

		accessions.add(a);
		genesysService.saveAccessions(institute, accessions);
	}

	private void deleteAccession(String instCode, String acceNumb, String genus, String species) throws NonUniqueAccessionException {
		final FaoInstitute institute = instituteService.getInstitute(instCode);
		final List<Accession> accessions = new ArrayList<Accession>();

		accessions.add(genesysService.getAccession(instCode, acceNumb, genus));

		genesysService.removeAccessions(institute, accessions);
	}

	@Test
	public void testBlankInput() {
		try {
			final InputStream str = new StringInputStream("");
			sgsvUpdate.importSGSVStream(str, "TEST");
		} catch (final Throwable e) {
			fail("Exception not expected");
		}
	}

	@Test
	public void testTooFewHeaders() {
		try {
			final InputStream str = new StringInputStream("a\tb\tc");
			sgsvUpdate.importSGSVStream(str, "TEST");
		} catch (final Throwable e) {
			fail("Exception not expected");
		}
	}

	@Test
	public void testImportNoSuchAccession() {
		try {
			final List<SGSVEntry> accns = new ArrayList<SGSVEntry>();
			final SGSVEntry entry = new SGSVEntry();
			entry.instCode = "INS002";
			entry.acceNumb = "NONE1";
			entry.genus = "Genus";
			entry.boxNo = "Box1";
			entry.quantity = 1.0f;
			accns.add(entry);

			sgsvUpdate.updateSvalbards(accns);

		} catch (final Throwable e) {
			fail("Exception not expected");
		}
	}

	@Test
	public void testImportSGSV() throws NonUniqueAccessionException {
		makeAccession("INS001", "TEST0", "Genus", "sp.");
		makeAccession("INS001", "TEST1", "Genus", "sp.");
		try {
			final List<SGSVEntry> accns = new ArrayList<SGSVEntry>();
			final SGSVEntry entry = new SGSVEntry();
			entry.instCode = "INS001";
			entry.acceNumb = "TEST1";
			entry.genus = "Genus";
			accns.add(entry);

			sgsvUpdate.updateSvalbards(accns);

		} catch (final Throwable e) {
			LOG.error(e, e);
			fail("Exception not expected");
		}

		assertTrue(svalbardRepository.findAll().size() > 0);

		deleteAccession("INS001", "TEST0", "Genus", "sp.");
		deleteAccession("INS001", "TEST1", "Genus", "sp.");
	}

	private String[] toEntry(String instCode, String acceNumb, String genus, String species, float quantity) {
		return new String[] { "a", instCode, "c", "d", acceNumb, "f", "g", Float.toString(quantity), "i", "j", "k", "l", "m", "n", "o", "p", genus, species,
				"s", "t", "u", "v", "w", "x", "y", "z", "a", "27" };
	}

	@Test
	public void testToEntry1() {
		final String[] entry = new String[] { "a", "b", "c", "d", "e", "f", "g", "1.0", "i", "j", "k", "l", "m", "n", "o", "p", "Genus", "sp.", "s", "t", "u",
				"v", "w", "x", "y", "z", "a", "27" };
		final SGSVEntry e = new SGSVEntry(entry);
		assertEntry(e);
	}

	@Test
	public void testToEntry2() {
		final String[] entry = toEntry("INS001", "TEST0", "Genus", "sp.", 1.0f);
		final SGSVEntry e = new SGSVEntry(entry);
		assertEntry(e);
	}

	void assertEntry(SGSVEntry e) {
		assertTrue(e != null);
		assertTrue(e.genus.equals("Genus"));
		assertTrue(e.species.equals("sp."));
		assertTrue(e.quantity == 1.0f);
	}

	@Test
	public void testWorkIt() throws NonUniqueAccessionException {
		makeAccession("INS001", "TEST0", "Genus", "sp.");
		makeAccession("INS001", "TEST1", "Genus", "sp.");

		try {
			final List<String[]> bulk = new ArrayList<String[]>();
			bulk.add(toEntry("INS001", "TEST0", "Genus", "sp.", 1.0f));
			bulk.add(toEntry("INS001", "TEST1", "Genus", "sp.", 1.0f));
			bulk.add(toEntry("INS001", "TEST2", "Genus", "sp.", 1.0f));
			bulk.add(toEntry("INS001", "TEST3", "Genus", "sp.", 1.0f));
			sgsvUpdate.workIt(bulk);
			assertTrue(bulk.size() == 0);
			bulk.add(toEntry("INS001", "TEST0", "Genus", "sp.", 1.0f));
			bulk.add(toEntry("INS001", "TEST1", "Genus", "sp.", 1.0f));
			sgsvUpdate.workIt(bulk);
			assertTrue(bulk.size() == 0);
			bulk.add(toEntry("INS001", "TEST2", "Genus", "sp.", 1.0f));
			bulk.add(toEntry("INS001", "TEST3", "Genus", "sp.", 1.0f));
			sgsvUpdate.workIt(bulk);
			assertTrue(bulk.size() == 0);
			sgsvUpdate.workIt(bulk);

		} catch (final Throwable e) {
			LOG.error(e, e);
			fail("Exception not expected");
		}

		final ThreadPoolTaskExecutor tpte = (ThreadPoolTaskExecutor) taskExecutor;

		while (tpte.getActiveCount() > 0) {
			try {
				LOG.info("Waiting for workers to finish. Active=" + tpte.getActiveCount());
				Thread.sleep(100);
			} catch (final InterruptedException e) {
			}
		}

		assertTrue(svalbardRepository.findAll().size() == 2);

		deleteAccession("INS001", "TEST0", "Genus", "sp.");
		deleteAccession("INS001", "TEST1", "Genus", "sp.");

		assertTrue(svalbardRepository.findAll().size() == 0);
	}
}
